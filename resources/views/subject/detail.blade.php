@extends('layouts.app')
@section('content')

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Data Subject</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <h3>Detail Subject</h3>
            </div>
        </div>
        <br>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Subject</label>
            <div class="col-sm-10">
                {{$subject->name}} 
            </div>
        </div>
        <div class="form-group row">
            <label for="slug" class="col-sm-2 col-form-label">Slug</label>
            <div class="col-sm-10">
                {{$subject->slug}} 
            </div>
        </div>
        <div class="form-group row">
            <label for="color" class="col-sm-2 col-form-label">Warna</label>
            <div class="col-sm-10">
                {{$subject->color}} 
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <a href="{{route('subject.index')}}" class="btn  btn-success">Kembali</a>
            </div>
        </div>

    </div>
</body>
</html>

@endsection