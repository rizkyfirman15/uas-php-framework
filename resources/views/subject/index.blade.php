@extends('layouts.app')
@section('content')
 
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Data Subject</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3> Daftar Subject</h3>
            </div>
            <div class="col-sm-2">
                <a class="btn btn-success" href="{{ route('subject.create')}}"> Tambah Subject </a>
            </div>
        </div> 
        <br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>        
        </div>
    @endif

    <table class="table table-striped">
      <thead>
        <tr>
            <th width="40px"><b>No.</b></th>
            <th width="180px">Subject</th>
            <th width="180px">Slug</th>
            <th width="180px">Warna</th>
            <th width="210px">Action</th>
        </tr>
      </thead>
        @foreach ($subjects as $subject) 
            <tr>
                <td><b>{{++$i}}.<b></td>
                <td>{{$subject->name}}</td>
                <td>{{$subject->slug}}</td>
                <td>{{$subject->color}}</td>
                <td>
                    <form action="{{ route('subject.destroy',$subject->id) }}" method="post">
                    <a class="btn btn-sm btn-success" href="{{ route('subject.show', $subject->id)}}">Show</a>
                    <a class="btn btn-sm btn-warning" href="{{ route('subject.edit', $subject->id)}}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>    
                </td>
            </tr>
        @endforeach
    </table>

    {!! $subjects->links() !!}
    </div>
    </body>

</html>

@endsection